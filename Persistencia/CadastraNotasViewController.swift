//
//  CadastraNotasViewController.swift
//  Persistencia
//
//  Created by iossenac on 12/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit
import CoreData

class CadastraNotasViewController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet weak var txtNota: UITextView!
    @IBAction func salvar(_ sender: Any) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let viewContext = delegate.persistentContainer.viewContext
        let notas = NSEntityDescription.insertNewObject(forEntityName: "Notas", into: viewContext)
        notas.setValue(txtNota.text, forKey: "texto")
        notas.setValue(NSDate(), forKey: "horario")
        do {
            try viewContext.save()
        } catch {
            print("Erro ao salvar")
        }
        print(txtNota.text)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
