//
//  ConfiguracoesViewController.swift
//  Persistencia
//
//  Created by iossenac on 12/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit

class ConfiguracoesViewController: UIViewController {

    @IBOutlet weak var txtNome: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let nome = UserDefaults.standard.object(forKey: "nome") {
            txtNome.text = nome as! String
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func salvar(_ sender: Any) {
        if let nome = txtNome.text {
            UserDefaults.standard.set(nome, forKey: "nome")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
